#!/bin/bash

MOUNT="/mnt/sda1"
PART="/dev/sda1"

echo "[run] umount $MOUNT"
umount $MOUNT
if [ $? -ne 0 ]
then
 echo "[error]"
 exit 1
fi

echo "[run] mkfs.ext4 $PART"
yes | mkfs.ext4 $PART
if [ $? -ne 0 ]
then
 echo "[error]"
 exit 1
fi

mkdir $MOUNT
echo "[run] mount $PART $MOUNT"
mount $PART $MOUNT
if [ $? -ne 0 ]
then
 echo "[error]"
 exit 1
fi

echo "[success]"
exit 0

