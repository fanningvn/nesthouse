#!/bin/sh

cd /root

#Set DHCP network
uci set network.lan.proto=dhcp
uci commit
/etc/init.d/network restart

sleep 3

#Install ak_flash
opkg install ak_flash_1.0.0-1_mipsel_24kc.ipk && rm ak_flash_1.0.0-1_mipsel_24kc.ipk
#Install nh_master
opkg install nh_master_1.0.0-1_mipsel_24kc.ipk && rm nh_master_1.0.0-1_mipsel_24kc.ipk

#Install scripts
cp asoundrc_Audio .asoundrc
cp sound_CM108B_OpenWrt.sh sound.sh

#Install openvpn
chmod +x /bin/app_mng.sh
chmod +x create_openvpn.sh
./create_openvpn.sh

