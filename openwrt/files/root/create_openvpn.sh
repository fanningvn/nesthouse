#!/bin/ash

echo 'modify interface...'
uci set network.vpn0=interface
uci set network.vpn0.ifname=tun0
uci set network.vpn0.proto=none
uci set network.vpn0.auto=1

echo 'modify firewall...'
uci set firewall.vpn=zone
uci set firewall.vpn.name=vpn
uci set firewall.vpn.network=vpn0
uci set firewall.vpn.input=ACCEPT #REJECT if using as WAN replacement
uci set firewall.vpn.forward=REJECT
uci set firewall.vpn.output=ACCEPT
uci set firewall.vpn.masq=1

uci set firewall.vpn_forwarding_lan_in=forwarding
uci set firewall.vpn_forwarding_lan_in.src=vpn
uci set firewall.vpn_forwarding_lan_in.dest=lan

uci set firewall.vpn_forwarding_lan_out=forwarding
uci set firewall.vpn_forwarding_lan_out.src=lan
uci set firewall.vpn_forwarding_lan_out.dest=vpn

echo 'commit interface...'
uci commit network
/etc/init.d/network reload

echo 'commit firewall...'
uci commit firewall
/etc/init.d/firewall reload

echo 'modify openvpn...'
echo > /etc/config/openvpn # clear the openvpn uci config
uci set openvpn.vpnclient="openvpn"
uci set openvpn.vpnclient.enabled="1"
uci set openvpn.vpnclient.config="/etc/openvpn/client1.ovpn"

echo 'commit openvpn...'
uci commit openvpn

echo 'disbale openvpn...'
/etc/init.d/openvpn disable
/etc/init.d/openvpn stop

