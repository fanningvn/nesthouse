#!/bin/sh /etc/rc.common
PACKAGE_NH_SETUP_ENV_PATH="/root/install_new_board_on_target.sh"
if [ -f "$PACKAGE_NH_SETUP_ENV_PATH" ]; then
    echo '[sleep 30s]'
    sleep 30
	
    echo "[run $PACKAGE_NH_SETUP_ENV_PATH]"
    chmod +x $PACKAGE_NH_SETUP_ENV_PATH	
    $PACKAGE_NH_SETUP_ENV_PATH
	
    echo "[rm $PACKAGE_NH_SETUP_ENV_PATH]"
    rm $PACKAGE_NH_SETUP_ENV_PATH
fi
exit 0
