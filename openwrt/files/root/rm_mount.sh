#!/bin/sh

#always mount point is /mnt/sda1
fix_device="sda1" 

#remove from /etc/config/fstab		
del_needed=$(uci show fstab | grep "/mnt/$fix_device" | awk -F '[' '{print $2}' | awk -F ']' '{print $1}' | sort -r )
[ -n "$del_needed" ] && {
	for i in $del_needed
	do
		uci delete fstab.@mount[$i]
	done
	uci commit fstab
}		

#umount old device
umount /mnt/$fix_device
rm -r /mnt/$fix_device


