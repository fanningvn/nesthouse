#!/bin/sh

#######################
#####  Define  ########
#######################
LOCK_FILE=/tmp/nh_master.lock

#######################
### Check lock file ###
#######################
if [ -f "$LOCK_FILE" ]; then
echo '[lock file exist]'
exit 0
fi

#######################                                                           
## Create lock file ##                                                           
#######################
echo '[create lock file]'
echo "`date`" >  $LOCK_FILE

#######################      
#####  Define  ########      
#######################
LOG_FILE=/root/nh_gateway/log

APP_NAME="nh_master"

PACKAGE_FIRMWARE_DIR="/root/nh_gateway/firmware/"         
PACKAGE_FIRMWARE_PATH="/root/nh_gateway/firmware/nesthouse.tar"                   

EXE_SCRIPT_UPDATE_PATH="/root/nh_gateway/firmware/exe_script_update.sh"

AK_FLASH_FIRMWARE_PATH="/root/nh_gateway/firmware/ak_flash_1.0.0-1_mipsel_24kc.ipk"
MASTER_FIRMWARE_PATH="/root/nh_gateway/firmware/nh_master_1.0.0-1_mipsel_24kc.ipk"
SLAVE_FIRMWARE_PATH="/root/nh_gateway/firmware/nesthouse_slave_app.bin"

RM_MOUNT_PATH="/root/rm_mount.sh"
MAP_SPEAKER_PATH="/root/sound.sh"

#######################                                                           
### Loop forever #####                                                            
#######################                                                           
while [ 1 ]; do
          
###########check app###################
if [ ! "$( pgrep "$APP_NAME")" ]; then

######check file firmware exist#########
if [ -f "$PACKAGE_FIRMWARE_PATH" ]; then                                       
                                                                          
echo '[package firmware exist]'
tar -xvf $PACKAGE_FIRMWARE_PATH -C $PACKAGE_FIRMWARE_DIR && rm $PACKAGE_FIRMWARE_PATH 
                                                                
fi                                                                               
                                                                                 
####check file exe_script_update exist###
if [ -f "$EXE_SCRIPT_UPDATE_PATH" ]; then

echo '[ak_flash firmware exist]'
chmod +x $EXE_SCRIPT_UPDATE_PATH
$EXE_SCRIPT_UPDATE_PATH && sleep 2 && rm $EXE_SCRIPT_UPDATE_PATH

fi

####check file master firmware exist###
if [ -f "$MASTER_FIRMWARE_PATH" ]; then                                               
                                                                                 
echo '[master firmware exist]'                                                     
opkg install $MASTER_FIRMWARE_PATH --force-reinstall && sleep 2 && rm $MASTER_FIRMWARE_PATH
                                                                   
fi                                                                 
                                                                   
####check file slave firmware exist###                            
if [ -f "$SLAVE_FIRMWARE_PATH" ]; then                                  
                                                                   
echo '[slave firmware exist]'                                        
retry_counter=0
while [ $retry_counter -lt 10 ]; do
   echo "retry_counter " $retry_counter
   ak_flash /dev/ttyS2 $SLAVE_FIRMWARE_PATH 134230016 && rm $SLAVE_FIRMWARE_PATH
   ret=$?
   echo $ret
   if [ $ret -eq 0 ]; then
       echo "ooo OK ooo"
       break
   else
       echo "xxx ERR xxx"
   fi
   retry_counter=`expr $retry_counter + 1`
done
fi

# NOTE: ak_flash must have install after flash slave firmware.
####check file ak_flash firmware exist###
if [ -f "$AK_FLASH_FIRMWARE_PATH" ]; then

echo '[ak_flash firmware exist]'
opkg install $AK_FLASH_FIRMWARE_PATH --force-reinstall && sleep 2 && rm $AK_FLASH_FIRMWARE_PATH

fi

#######################
##### Run app     #####
#######################
if [ -n "$(ls -A /mnt/sda1)"]; then                                          
echo '[run rm_mount.sh]'                                       
chmod +x $RM_MOUNT_PATH
$RM_MOUNT_PATH > /dev/null 2>&1
fi

echo '[run map speaker]'                                                     
chmod +x $MAP_SPEAKER_PATH                                   
$MAP_SPEAKER_PATH > /dev/null 2>&1

echo '[run app]'
cd $HOME
$APP_NAME > /dev/null 2>&1
echo '[exit app]'

###########end check app############
fi

echo '[sleep 10s]'
sleep 10

#######################
### End loop forever ##
#######################
done

echo '[remove lock file]'
rm $LOCK_FILE

exit 0

