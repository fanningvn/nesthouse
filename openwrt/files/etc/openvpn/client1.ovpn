##############################################
# Sample client-side OpenVPN 2.0 config file #
# for connecting to multi-client server.     #
#                                            #
# This configuration can be used by multiple #
# clients, however each client should have   #
# its own cert and key files.                #
#                                            #
# On Windows, you might want to rename this  #
# file so it has a .ovpn extension           #
##############################################

# Specify that we are a client and that we
# will be pulling certain config file directives
# from the server.
client

# Use the same setting as you are using on
# the server.
# On most systems, the VPN will not function
# unless you partially or fully disable
# the firewall for the TUN/TAP interface.
;dev tap
dev tun

# Windows needs the TAP-Win32 adapter name
# from the Network Connections panel
# if you have more than one.  On XP SP2,
# you may need to disable the firewall
# for the TAP adapter.
;dev-node MyTap

# Are we connecting to a TCP or
# UDP server?  Use the same setting as
# on the server.
;proto tcp
proto udp

# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote 171.244.43.37 1194
;remote my-server-2 1194

# Choose a random host from the remote
# list for load-balancing.  Otherwise
# try hosts in the order specified.
;remote-random

# Keep trying indefinitely to resolve the
# host name of the OpenVPN server.  Very useful
# on machines which are not permanently connected
# to the internet such as laptops.
resolv-retry infinite

# Most clients don't need to bind to
# a specific local port number.
nobind

# Downgrade privileges after initialization (non-Windows only)
user nobody
group nogroup

# Try to preserve some state across restarts.
persist-key
persist-tun

# If you are connecting through an
# HTTP proxy to reach the actual OpenVPN
# server, put the proxy server/IP and
# port number here.  See the man page
# if your proxy server requires
# authentication.
;http-proxy-retry # retry on connection failures
;http-proxy [proxy server] [proxy port #]

# Wireless networks often produce a lot
# of duplicate packets.  Set this flag
# to silence duplicate packet warnings.
;mute-replay-warnings

# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
ca ca.crt
cert client.crt
key client.key

# Verify server certificate by checking that the
# certicate has the correct key usage set.
# This is an important precaution to protect against
# a potential attack discussed here:
#  http://openvpn.net/howto.html#mitm
#
# To use this feature, you will need to generate
# your server certificates with the keyUsage set to
#   digitalSignature, keyEncipherment
# and the extendedKeyUsage to
#   serverAuth
# EasyRSA can do this for you.
remote-cert-tls server

# If a tls-auth key is used on the server
# then every client must also have the key.
;tls-auth ta.key 1

# Select a cryptographic cipher.
# If the cipher option is used on the server
# then you must also specify it here.
;cipher x
cipher AES-128-CBC
auth SHA256
key-direction 1

# Enable compression on the VPN link.
# Don't enable this unless it is also
# enabled in the server config file.
comp-lzo

# Set log file verbosity.
verb 3

# Silence repeating messages
;mute 20
<ca>
-----BEGIN CERTIFICATE-----
MIIE0DCCA7igAwIBAgIJAOWqWMuQYs6LMA0GCSqGSIb3DQEBCwUAMIGgMQswCQYD
VQQGEwJWTjELMAkGA1UECBMCU0cxEDAOBgNVBAcTB1NhaSBHb24xDTALBgNVBAoT
BEhvbWUxHTAbBgNVBAsTFE15T3JnYW5pemF0aW9uYWxVbml0MRAwDgYDVQQDEwdI
b21lIENBMQ8wDQYDVQQpEwZzZXJ2ZXIxITAfBgkqhkiG9w0BCQEWEm1lQG15aG9z
dC5teWRvbWFpbjAeFw0xODA2MjMwNDE1MzJaFw0yODA2MjAwNDE1MzJaMIGgMQsw
CQYDVQQGEwJWTjELMAkGA1UECBMCU0cxEDAOBgNVBAcTB1NhaSBHb24xDTALBgNV
BAoTBEhvbWUxHTAbBgNVBAsTFE15T3JnYW5pemF0aW9uYWxVbml0MRAwDgYDVQQD
EwdIb21lIENBMQ8wDQYDVQQpEwZzZXJ2ZXIxITAfBgkqhkiG9w0BCQEWEm1lQG15
aG9zdC5teWRvbWFpbjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMJR
08DpM5y4t1bHF0V1rDPnRdMD9Ja+KQjiJu700uWZ8KQaKl95B/aV1kM3n2tzvVy6
gWjzzD/ofD4P7oFGOajn8Lr1qjR150EIB4erLfu3kIaR6ZxBGkj3jqB8unEN+L08
/dJU4g6JuubAqpdi2tGm1z3/BMVQ/kAyfGBd4OlJkgIp+PN+wCwmS3jM1X3ndawg
PoixoZvEQS19dHtHMzE0c0pDd8PG8gG8tx5aVp41GBCILpF1EF0DjR3eFdSfcQAS
/XLBaq8GM8F9a4Eq45a28BFybjKth25MjyBBwKXWx+Cqgv9gZUPFbZwdEvlQO7hk
EdceB4qYxqccwPzWotkCAwEAAaOCAQkwggEFMB0GA1UdDgQWBBTwPZGpA6+r6dOk
ngucRN89ZWgpbjCB1QYDVR0jBIHNMIHKgBTwPZGpA6+r6dOkngucRN89ZWgpbqGB
pqSBozCBoDELMAkGA1UEBhMCVk4xCzAJBgNVBAgTAlNHMRAwDgYDVQQHEwdTYWkg
R29uMQ0wCwYDVQQKEwRIb21lMR0wGwYDVQQLExRNeU9yZ2FuaXphdGlvbmFsVW5p
dDEQMA4GA1UEAxMHSG9tZSBDQTEPMA0GA1UEKRMGc2VydmVyMSEwHwYJKoZIhvcN
AQkBFhJtZUBteWhvc3QubXlkb21haW6CCQDlqljLkGLOizAMBgNVHRMEBTADAQH/
MA0GCSqGSIb3DQEBCwUAA4IBAQCurwmjyikTs3pB2w2qpDMNwRCik7iVuZW6zK+c
r/TRwMyQK4L/57cuQJTWRURgH02fE6EwNj3m17sq+JGH5+7sHdDYlh93vyfo41A6
lGSBHQ9e7F6h7f3Fu0zrtKuivhMsOgXvQ8KiiOV+ruRewutj5icMxa8L0Wo7iQt8
FYm185vxuRJXfVn/W7FXeeyPV0WO+YgH333633DqOzEaxAa85O/M/T/yDeQhkXhU
4ESwcS0V1rZYchzN7ksJr9UGgJB9+bGWJmaDHBLLHT4Z7gn5N06XdxR2nSFk7BRg
I1HmBEwVQYN5zEyeHrHR/d6+7LnD18Sbqhrf0YcvISWpVw+Z
-----END CERTIFICATE-----
</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 2 (0x2)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=VN, ST=SG, L=Sai Gon, O=Home, OU=MyOrganizationalUnit, CN=Home CA/name=server/emailAddress=me@myhost.mydomain
        Validity
            Not Before: Jun 23 04:30:12 2018 GMT
            Not After : Jun 20 04:30:12 2028 GMT
        Subject: C=VN, ST=SG, L=Sai Gon, O=Home, OU=MyOrganizationalUnit, CN=client1/name=server/emailAddress=me@myhost.mydomain
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:cd:5c:50:4c:7d:13:29:7e:8f:2f:f7:19:dd:60:
                    d5:3e:76:a4:23:5d:87:a2:e3:37:16:75:fa:d3:be:
                    22:c4:50:60:35:bd:e4:45:07:30:98:6b:0a:22:36:
                    77:2a:79:a4:f5:68:9d:d7:6e:d9:e5:f6:a3:17:e9:
                    2a:45:64:8f:0c:5b:eb:43:11:42:a4:af:b8:97:c2:
                    eb:49:de:7d:0c:78:ba:cf:08:4b:4f:b4:56:cc:2d:
                    5d:36:b2:5e:36:30:6e:b2:6e:03:f6:3b:ed:71:34:
                    b5:c0:fd:69:6f:36:bd:98:0a:b8:39:4d:ea:c1:d2:
                    92:fc:0b:d4:0d:08:a7:74:42:c9:84:5e:36:d7:99:
                    5d:07:fe:ff:82:82:3f:b7:57:27:b9:68:1c:3e:c1:
                    3a:b8:17:ac:bc:99:3c:d4:8f:7c:2c:97:e7:04:5f:
                    1a:99:82:2b:3c:80:e9:84:53:42:65:5f:0d:3a:ff:
                    71:83:67:1d:a6:5e:e9:ee:14:ff:d1:6c:31:40:c7:
                    95:2f:88:e2:f5:3c:e9:e9:80:8f:29:5d:88:31:0a:
                    4c:92:b1:f9:7d:48:09:9c:6a:f6:eb:59:33:8b:f2:
                    63:91:1f:5a:25:41:96:a7:11:b1:67:73:c9:82:6c:
                    47:50:8e:dc:e2:62:80:20:9f:97:a4:99:32:da:13:
                    7d:21
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                Easy-RSA Generated Certificate
            X509v3 Subject Key Identifier: 
                CE:3B:F1:1F:E3:4A:CC:0C:0D:24:83:51:6F:3D:2C:EF:6F:BC:89:2D
            X509v3 Authority Key Identifier: 
                keyid:F0:3D:91:A9:03:AF:AB:E9:D3:A4:9E:0B:9C:44:DF:3D:65:68:29:6E
                DirName:/C=VN/ST=SG/L=Sai Gon/O=Home/OU=MyOrganizationalUnit/CN=Home CA/name=server/emailAddress=me@myhost.mydomain
                serial:E5:AA:58:CB:90:62:CE:8B

            X509v3 Extended Key Usage: 
                TLS Web Client Authentication
            X509v3 Key Usage: 
                Digital Signature
            X509v3 Subject Alternative Name: 
                DNS:client1
    Signature Algorithm: sha256WithRSAEncryption
         8c:68:ef:6d:e4:8d:3c:6a:4e:80:a4:50:ae:a4:d8:f1:19:99:
         ea:bb:7a:79:fb:30:3d:2c:7a:04:6e:18:a0:ea:d4:cb:6c:8f:
         8c:ef:4f:8c:c8:ee:f7:63:e0:4d:1a:ed:48:be:b3:55:14:ab:
         d5:3a:c4:34:92:ad:b0:86:7a:40:d2:e3:80:03:fc:58:13:93:
         77:7d:de:16:8a:9a:a1:d5:9b:b0:c4:d6:a2:54:9e:3e:a7:f3:
         a8:e6:6b:f0:47:7f:41:11:9c:59:fc:ed:e9:da:8d:07:d5:bd:
         77:5f:5a:c2:b5:02:bb:45:9c:fb:3a:ba:8d:cb:e2:41:a9:a9:
         29:a7:4b:46:b4:0e:68:ad:b7:95:a1:d3:32:da:14:9f:5d:99:
         f5:8b:cd:82:c7:95:65:44:31:9b:0a:18:a3:d5:89:4d:08:70:
         c3:0c:85:0f:3c:9c:a8:7f:88:14:4c:68:ad:7c:d8:a3:91:a2:
         5b:4f:56:a7:31:47:08:27:bd:f3:db:20:5f:08:0b:3b:8d:34:
         fe:ab:e7:7a:2f:19:09:54:9b:0b:2c:59:6c:8f:7d:c3:a0:5e:
         2d:49:79:65:7c:95:c8:ec:73:12:4f:45:62:4c:c1:5c:f1:83:
         b1:2f:b8:d7:e3:5e:1a:c8:19:fc:5a:44:55:99:e7:f3:65:b8:
         a1:74:6c:39
-----BEGIN CERTIFICATE-----
MIIFKjCCBBKgAwIBAgIBAjANBgkqhkiG9w0BAQsFADCBoDELMAkGA1UEBhMCVk4x
CzAJBgNVBAgTAlNHMRAwDgYDVQQHEwdTYWkgR29uMQ0wCwYDVQQKEwRIb21lMR0w
GwYDVQQLExRNeU9yZ2FuaXphdGlvbmFsVW5pdDEQMA4GA1UEAxMHSG9tZSBDQTEP
MA0GA1UEKRMGc2VydmVyMSEwHwYJKoZIhvcNAQkBFhJtZUBteWhvc3QubXlkb21h
aW4wHhcNMTgwNjIzMDQzMDEyWhcNMjgwNjIwMDQzMDEyWjCBoDELMAkGA1UEBhMC
Vk4xCzAJBgNVBAgTAlNHMRAwDgYDVQQHEwdTYWkgR29uMQ0wCwYDVQQKEwRIb21l
MR0wGwYDVQQLExRNeU9yZ2FuaXphdGlvbmFsVW5pdDEQMA4GA1UEAxMHY2xpZW50
MTEPMA0GA1UEKRMGc2VydmVyMSEwHwYJKoZIhvcNAQkBFhJtZUBteWhvc3QubXlk
b21haW4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDNXFBMfRMpfo8v
9xndYNU+dqQjXYei4zcWdfrTviLEUGA1veRFBzCYawoiNncqeaT1aJ3Xbtnl9qMX
6SpFZI8MW+tDEUKkr7iXwutJ3n0MeLrPCEtPtFbMLV02sl42MG6ybgP2O+1xNLXA
/WlvNr2YCrg5TerB0pL8C9QNCKd0QsmEXjbXmV0H/v+Cgj+3Vye5aBw+wTq4F6y8
mTzUj3wsl+cEXxqZgis8gOmEU0JlXw06/3GDZx2mXunuFP/RbDFAx5UviOL1POnp
gI8pXYgxCkySsfl9SAmcavbrWTOL8mORH1olQZanEbFnc8mCbEdQjtziYoAgn5ek
mTLaE30hAgMBAAGjggFrMIIBZzAJBgNVHRMEAjAAMC0GCWCGSAGG+EIBDQQgFh5F
YXN5LVJTQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFM478R/jSswM
DSSDUW89LO9vvIktMIHVBgNVHSMEgc0wgcqAFPA9kakDr6vp06SeC5xE3z1laClu
oYGmpIGjMIGgMQswCQYDVQQGEwJWTjELMAkGA1UECBMCU0cxEDAOBgNVBAcTB1Nh
aSBHb24xDTALBgNVBAoTBEhvbWUxHTAbBgNVBAsTFE15T3JnYW5pemF0aW9uYWxV
bml0MRAwDgYDVQQDEwdIb21lIENBMQ8wDQYDVQQpEwZzZXJ2ZXIxITAfBgkqhkiG
9w0BCQEWEm1lQG15aG9zdC5teWRvbWFpboIJAOWqWMuQYs6LMBMGA1UdJQQMMAoG
CCsGAQUFBwMCMAsGA1UdDwQEAwIHgDASBgNVHREECzAJggdjbGllbnQxMA0GCSqG
SIb3DQEBCwUAA4IBAQCMaO9t5I08ak6ApFCupNjxGZnqu3p5+zA9LHoEbhig6tTL
bI+M70+MyO73Y+BNGu1IvrNVFKvVOsQ0kq2whnpA0uOAA/xYE5N3fd4Wipqh1Zuw
xNaiVJ4+p/Oo5mvwR39BEZxZ/O3p2o0H1b13X1rCtQK7RZz7OrqNy+JBqakpp0tG
tA5orbeVodMy2hSfXZn1i82Cx5VlRDGbChij1YlNCHDDDIUPPJyof4gUTGitfNij
kaJbT1anMUcIJ73z2yBfCAs7jTT+q+d6LxkJVJsLLFlsj33DoF4tSXllfJXI7HMS
T0ViTMFc8YOxL7jX414ayBn8WkRVmefzZbihdGw5
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDNXFBMfRMpfo8v
9xndYNU+dqQjXYei4zcWdfrTviLEUGA1veRFBzCYawoiNncqeaT1aJ3Xbtnl9qMX
6SpFZI8MW+tDEUKkr7iXwutJ3n0MeLrPCEtPtFbMLV02sl42MG6ybgP2O+1xNLXA
/WlvNr2YCrg5TerB0pL8C9QNCKd0QsmEXjbXmV0H/v+Cgj+3Vye5aBw+wTq4F6y8
mTzUj3wsl+cEXxqZgis8gOmEU0JlXw06/3GDZx2mXunuFP/RbDFAx5UviOL1POnp
gI8pXYgxCkySsfl9SAmcavbrWTOL8mORH1olQZanEbFnc8mCbEdQjtziYoAgn5ek
mTLaE30hAgMBAAECggEAOynqVG8AU0cL95SoFfhs1ycVGL88cXgmickJr3Eq0QYb
AcCogB6XKIJGFB8+67TKCo3OU5+zFSaeTCg3rnBPKg9dUyAo5AtgEhDs+oYn3qz1
nOnV0KuzCfXT0gtHSzNe4PcDGzCesqut5WDcvFa3/pKwCophJOAGyqiZufuRStPi
2N5lncWb63ErUTj1RuFHVN76L3tKct7D7jdaq8HMoCE0a+HxE/CSejGs+xBOxGyp
Ff+2lrAindlgVf4UyabcnUsdcOKIrVyRmntusZIgUxkSDyEeVJF6HklDXs/z1xfZ
kZovqeF6LvkrHlPMlOQyoWziXNe63HLcSmTPdcZDlQKBgQDpxtoFjTW+TsKKrcmI
eip0Z+8CNUtNwMws1fZ19/d3GAtID4IlsZkPwIG3r5Vcd/kPIzg+Wao7GFBllWoW
1i7H+5fFJ+YmqgqYrcyG7ac8mxx6niFiGaHpz+1q1f+NoUnp5QO/7iPje45TQIb9
z9h8EkXFlxpDL9xZ4BL2U0g0EwKBgQDg4e51U0tJWCfE6pMEHlI9yk2a3VWhR5mB
RUVi/M19YwBuWQP+Dz+zVJD86lK7taBGKVbYZXQjkZGIAI7jLpPX/Nrz8lJoH5M/
jVbQevA0mc0nw9ippLSpzS0+hvQO5nkC3Z0VXgAL3T+E774Z4XPQzrRsfIoRr6T7
ZRV/pbKoewKBgAJIbtifCGdTXZL9H3q1trRsT2k2HAYmW9gUPtPoGAKRuYp6nErB
8Ty2JI4mqM6+XTBIp8P0HDV3h1F2pBKVBN/vFQxX12eJQZE8IUMwDvIIkb6gzLKo
jWw7G5ZvgI9hg9dE5UR6jt8p1bn6Z1cTMIoFo6jKUPdXAE/gYE8HgS0BAoGAQ7cE
bMO2CdHHt93Cgxz2OrDuCVHYdQXwTSKksBfbcIQdHx0ivJ+u2LsAJYgRfYBFUJsm
EWcLP6KXpdI1NGdUEVeMPqSa98t6NUrD92btaYleYzjvxrVXyUGE/Cz61HnFun0s
zcBOV0Q52jeukHB4xUlYvsoJcSXokeJwRM2KLH0CgYEA6JnxK7VHNqEnySqGNK1y
pFj03BfjXjmrUBUTqCUvnp5kw9jb7hITDzM6TcSe6BMjCeiYxCI/ukktZrqnHTo4
fGD4KvS0Hj64PJTr/mMIoOyEHfvI6YCkvFLWjKOlBnR1m0GfouYVh8ICLslwP5hy
RWK4tedA9+MPUOxFr1ILTzY=
-----END PRIVATE KEY-----
</key>
<tls-auth>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
4eda86c069a6baaf19829520aaaf2acf
a6f73c5a4ce4e4a8d3675ca22bae6fc9
c368b3f5dbf23c864ba97bd3ecdde592
c5dd31360a280d990cb6a982c4d9b202
07814fde75a567518c176028b068e5e8
1691abf9f2c521b83d6705a5fdc84f48
6e345ea5d5cf0ea5df14c8a12ed4783a
65df169089c2eb078d137c4591fcbc5d
87ee2366e737952498cd34ecdbee00fc
24bfa1d58a634ae80b47c72fb7aad431
c2b5f192e966c2d6338fcfbeb22c9bee
ca6371f62d10b77e43b57faddd0c613d
f9b20e745f94a82496c5c1dc0a1a5b03
c4b77962c52d9b4b02d701663a8d3a18
f545a3516746b18e88210000deff0d7a
7cc39314325a445e053f2e1365e8972d
-----END OpenVPN Static key V1-----
</tls-auth>
